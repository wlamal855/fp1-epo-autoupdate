/*
 * Copyright (C) 2014 Ragnar Bonk, fp1-epo-autoupdate@b0nk.de
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.b0nk.fp1_epo_autoupdate;

import java.io.File;

public final class Settings {
    public static final String FILE_NAME = "settings";
    public static final String LOG_DEBUG_TAG = "FP1-EPO-Autoupdate";
    public static final long STATUS_UPDATE_INTERVAL = 2000;
    public static final long CLEAR_NOTIFICATIONS_TIMEOUT = 3000;
    public static final long OUTDATED_INTERVAL = 2592000000L; // 30 days

    public static final String HTTP_SERVER_KEY = "de.b0nk.fp1_epo_autoupdate.Settings.httpServer";
    public static final String HTTP_SERVER_DEFAULT = "epodownload.mediatek.com";

    public static final String HTTP_PORT_KEY = "de.b0nk.fp1_epo_autoupdate.Settings.httpPort";
    public static final String HTTP_PORT_DEFAULT = "";

    public static final String HTTP_PATH_KEY = "de.b0nk.fp1_epo_autoupdate.Settings.httpPath";
    public static final String HTTP_PATH_DEFAULT = "/EPO.DAT";

    public static final String COMMON_UPDATE_INTERVAL_KEY = "de.b0nk.fp1_epo_autoupdate.Settings.commonUpdateInterval";
    public static final String COMMON_UPDATE_INTERVAL_DEFAULT = "7200";    // 5 days

    public static final String COMMON_WIFI_ONLY_KEY = "de.b0nk.fp1_epo_autoupdate.Settings.commonWifiOnly";
    public static final String COMMON_WIFI_ONLY_DEFAULT = "true";

    public static final String COMMON_LAST_UPDATE_KEY = "de.b0nk.fp1_epo_autoupdate.Settings.commonLastRun";
    public static final String COMMON_LAST_UPDATE_DEFAULT = Long.toString(new File("/data/misc/EPO.DAT").lastModified());
}
