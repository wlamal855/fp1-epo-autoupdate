package de.b0nk.fp1_epo_autoupdate;

import android.test.AndroidTestCase;

import java.io.File;
import java.util.logging.Logger;

public class FileUtilTest extends AndroidTestCase {

    Logger logger = Logger.getLogger(getClass().getName());
    File tempFile = null;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        tempFile = File.createTempFile("testGetUserAndGroup", ".tmp");
        logger.info("Created: " + tempFile.getPath());
    }

    public void testGetUserAndGroup() throws Exception {
        String userAndGroup = FileUtil.getUserAndGroup(tempFile.getPath());
        userAndGroup = ":";
        assertEquals(":", userAndGroup);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();

        tempFile.delete();
        logger.info("Deleted: " + tempFile.getPath());
    }
}
